package com.example.crime

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {
    lateinit var viewModel: CrimeViewModel

    lateinit var btnChangeTime: Button
    lateinit var tvTime: TextView

    lateinit var btnChangeDate: Button
    lateinit var tvDate: TextView

    lateinit var crime: Crime
    lateinit var btnReloadData: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        crime = Crime("", "")
        viewModel = ViewModelProvider(this).get(CrimeViewModel::class.java)

        tvTime = findViewById(R.id.tvTime)
        btnChangeTime = findViewById(R.id.btnChangeTime)

        tvDate = findViewById(R.id.tvDate)
        btnChangeDate = findViewById(R.id.btnChangeDate)

        btnReloadData = findViewById(R.id.btnReloadData)

        btnChangeTime.setOnClickListener {
            val dialogT = TimePickerFragment()
            dialogT.show(supportFragmentManager, "TimePickerDialog")
        }
        btnChangeDate.setOnClickListener {
            val dialogD = DatePickerFragment()
            dialogD.show(supportFragmentManager, "DatePickerDialog")
        }

        btnReloadData.setOnClickListener {
            viewModel.time.observe(this, Observer {
                crime.time = it
            })
            tvTime.setText("Время: ${crime.time}")
            viewModel.date.observe(this, Observer {
                crime.date = it
            })
            tvDate.setText("Дата: ${crime.date}")
        }
    }



}