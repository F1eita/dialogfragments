package com.example.crime

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class CrimeViewModel: ViewModel() {
    private val _time = MutableLiveData<String>("")
    val time: LiveData<String> = _time
    private val _date = MutableLiveData<String>("")
    val date: LiveData<String> = _date
    fun setCurrentTime(time: String){
        _time.value = time
    }
    fun setCurrentDate(date: String){
        _date.value = date
    }
}