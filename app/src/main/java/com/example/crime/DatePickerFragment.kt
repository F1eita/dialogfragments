package com.example.crime

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels

class DatePickerFragment : DialogFragment() {

    val viewModel: CrimeViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_date_picker, container, false)
        val editDate: EditText = fragmentLayout.findViewById(R.id.edDate)
        val btnSaveDate: Button = fragmentLayout.findViewById(R.id.btnSaveDate)
        btnSaveDate.setOnClickListener {
            viewModel.setCurrentDate(editDate.getText().toString())
        }
        return fragmentLayout
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            DatePickerFragment()

    }
}