package com.example.crime

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.fragment.app.activityViewModels

class TimePickerFragment : DialogFragment() {

    val viewModel: CrimeViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_time_picker, container, false)
        val editTime: EditText = fragmentLayout.findViewById(R.id.edTime)
        val btnSaveTime: Button = fragmentLayout.findViewById(R.id.btnSaveTime)
        btnSaveTime.setOnClickListener {
            viewModel.setCurrentTime(editTime.getText().toString())
        }
        return fragmentLayout
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            TimePickerFragment()
    }
}